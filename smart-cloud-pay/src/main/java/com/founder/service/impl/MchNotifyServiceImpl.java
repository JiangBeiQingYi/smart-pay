package com.founder.service.impl;

import com.founder.core.dao.MchNotifyRespository;
import com.founder.core.domain.MchNotify;
import com.founder.service.IMchNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MchNotifyServiceImpl implements IMchNotifyService {

    @Autowired
    MchNotifyRespository mchNotifyRespository;

    @Override
    public MchNotify selectMchNotify(String orderId) {
        Optional<MchNotify> optional = mchNotifyRespository.findById(orderId);
        return optional.isPresent() ? optional.get() : null;
    }

    @Override
    public int updateNotify4Count(String orderId, Integer cnt) {
        MchNotify mchNotify = this.selectMchNotify(orderId);
        if (mchNotify != null){
            mchNotify.setNotifyCount(cnt);
            mchNotifyRespository.save(mchNotify);
            return 1;
        }
        return 0;
    }

    @Override
    public int saveMchNotify(MchNotify mchNotify) {
        mchNotify.setUpdateTime(new Date());
        mchNotifyRespository.save(mchNotify);
        return 1;
    }
}
