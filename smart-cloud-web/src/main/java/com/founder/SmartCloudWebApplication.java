package com.founder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartCloudWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartCloudWebApplication.class, args);
    }
}
